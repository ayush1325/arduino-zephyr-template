# Arduino Core API template for Zephyr

This is a template to allow quick and easy setup to use Arduino APIs on Zephyr supported boards. This has been tested for beagleconnect_freedom although should work with most other boards supported by [Arduino Core API module for Zephyr](https://github.com/zephyrproject-rtos/gsoc-2022-arduino-core).

# Setup

If this is your first time using zephyr, [Install Zephyr SDK](https://docs.zephyrproject.org/latest/develop/getting_started/index.html#install-the-zephyr-sdk) before following the steps below.

1. Create a workspace folder:

```shell
mkdir arduino-workspace
cd arduino-workspace
```

2. Setup virtualenv

```shell
python -m venv .venv
source .venv/bin/activate
pip install west
```

3. Setup Zephyr app:

```shell
west init -m https://openbeagle.org/beagleconnect/arduino-zephyr-template .
west update
```

4. Setup Arduino module

```shell
ln -srf modules/lib/ArduinoCore-API/api modules/lib/Arduino-Zephyr-API/cores/arduino/.
```

5. Install python deps

```shell
pip install -r zephyr/scripts/requirements-base.txt
```

# Build

```shell
west build -b beagleconnect_freedom arduino-zephyr-template -p
```

# Flash

```shell
west flash
```

# Helpful Links
- [Arduino Core API module for Zephyr](https://github.com/zephyrproject-rtos/gsoc-2022-arduino-core)
